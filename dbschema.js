let db = {
    users: [{
        userId: '',
        email: 'user@email.com',
        handle: 'user',
        createdAt: '2019-07-25T11:40:40.443Z',
        imageUrl: 'image/sdfsajkdgd/sjdghsajdf',
        bio: 'Hello, my name is user, nice to meet you',
        website: 'https://user.com',
        location: 'Istanbul, TR'
    }],
    screams : [{
        userHandle: 'user',
        body: 'this is the sccream body',
        createdAt: '2019-07-25T11:40:40.443Z',
        likeCount : 5,
        commentCount: 2
    }],
    comments : [{
        userHandle: 'new',
        screamId: 'WqxlfDpVrvB3YuOowqX6',
        body: 'My Fisrt Scream',
        createdAt: '2019-08-02T15:06:04.145Z'
    }],
    notifications: [{
        recipient: 'user',
        sender: 'wawle',
        read: 'true | false',
        screamId : 'asdfghjkl',
        type: 'like | comment',
        createdAt: '2019-08-02T15:06:04.145Z'
    }]
};

const userDetails = {
    //Redux Data
    credentials : {
        userId: 'SDHGSAD6FDH439UF9JJ',
        email : 'user@email.com',
        handle: 'user',
        createdAt : '2019-07-25T11:40:40.443Z',
        imageUrl: 'image/sdjfhsgtrhry/sdnvsdfgdf',
        bio: 'Hi',
        website : 'https://user.com',
        location: 'IStanbul, TR'
    },
    likes : [
        {
            userHandle: 'user',
            screamId: 'ihfesgf983jfi',
        },
        {
            userHandle : 'user',
            screamId: 'sjfvherp834fi2039'
        }
    ]
}